#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <climits>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;


struct compare {
	bool operator()(const std::string& first, const std::string& second) {
		return first.size() < second.size();
	}
};

void quicksortLength(vector<string>& w){
	compare c;
	std::sort(w.begin(), w.end(), c);
}
int longest_chain(vector<string> w){
	quicksortLength(w);
	map<string, int> mp;
	for(int i = 0; i < w.size(); i++){
		int max = 0;
		for(int j = 0; j < w[i].length(); j++){
			string substr = w[i].substr(0, j) + w[i].substr(j+1, w[i].length() - 1 - j);
			if(mp.find(substr) != mp.end() && mp[substr] > max)
				max = mp[substr];
		}
		max++;
		mp[w[i]] = max;
	}
	int max = 0;
	for(map<string, int>::iterator it = mp.begin(); it != mp.end(); it++){
		if(it->second > max)
			max = it->second;
	}
	return max;
}

int main() {
	int res;
	int _w_size = 0;
	cin >> _w_size;
	cin.ignore (std::numeric_limits<std::streamsize>::max(), '\n'); 
	vector<string> _w;
	string _w_item;
	for(int _w_i=0; _w_i<_w_size; _w_i++) {
		getline(cin, _w_item);
		_w.push_back(_w_item);
	}
//	sortLength(_w);
//	for(int i = 0; i < _w.size(); i++)
//		cout << _w[i] << endl;
	res = longest_chain(_w);
	cout << res << endl;

	return 0;
}
