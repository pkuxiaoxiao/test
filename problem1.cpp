#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <climits>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;

bool isIsolated(int index, vector<string> friends){
	for(int j = 0; j < friends.size(); j++){
		if(j != index && friends[index][j] == 'Y')
			return false;
	}
	return true;
}
vector<vector<int> > findPairs(vector<string> friends){
	int N = friends.size();
	vector<vector<int> > pairs;
	for(int i = 0; i < N; i++){
		if(isIsolated(i, friends)){
			vector<int> pair;
			pair.push_back(i);
			pair.push_back(i);
			pairs.push_back(pair);
		}
		else{
			for(int j = i + 1; j < N; j++){
				if(friends[i][j] == 'Y'){
					vector<int> pair;
					pair.push_back(i);
					pair.push_back(j);
					pairs.push_back(pair);
				}
			}
		}
	}
	return pairs;
}

bool shareFriend(vector<int> a, vector<int> b){
	map<int, int> mp;
   	for(int i = 0; i < a.size(); i++)
		mp[a[i]] = 1;
	for(int j = 0; j < b.size(); j++){
		if(mp.find(b[j]) != mp.end())
			return true;	
	}
	return false;
}

vector<int> merge(vector<int> a, vector<int> b){
	vector<int> merged(a.size() + b.size());
	std::sort(a.begin(), a.end());
	std::sort(b.begin(), b.end());
	std::merge(a.begin(), a.end(), b.begin(), b.end(), merged.begin());
	std::sort(merged.begin(), merged.end());
	merged.erase(unique(merged.begin(), merged.end()), merged.end());
	return merged;	
}

vector<vector<int> >  unionCircles(vector<vector<int> > circles){
	vector<vector<int> > result;
	result.push_back(circles[0]);
	for(int i = 1; i < circles.size(); i++){
		bool shareFriends = false;
		for(int j = 0; j < i; j++){
			if(shareFriend(circles[i], circles[j])){
				shareFriends = true;
				circles[j] = merge(circles[i], circles[j]);
			}
		}
		if(!shareFriends)
			result.push_back(circles[i]);
	}
	return result;
}
int friendCircles(vector<string> friends){
	vector<vector<int> > pairs = findPairs(friends);
	vector<vector<int> > unioned = unionCircles(pairs);
    return unioned.size();
}
int main() {
	int res;

	int _friends_size;
	cin >> _friends_size;
	cin.ignore (std::numeric_limits<std::streamsize>::max(), '\n'); 
	vector<string> _friends;
	string _friends_item;
	for(int _friends_i=0; _friends_i<_friends_size; _friends_i++) {
		getline(cin, _friends_item);
		_friends.push_back(_friends_item);
	}

	vector<vector<int> > pairs = findPairs(_friends);
//	for(int i = 0; i < pairs.size(); i++){
//		for(int j = 0; j < 2; j++)
//			cout<<pairs[i][j]<<' ';
//		cout<<endl;
//	}
//	vector<vector<int> > u = unionPairs(pairs);
//	for(int i = 0; i < u.size(); i++){
//		cout<<u[i][0]<<' '<<u[i][1]<<endl;
//	}
	res = friendCircles(_friends);
	cout << res << endl;

	return 0;
}
