/**
 * Created by xiaoxiao on 16/2/8.
 */
import java.io.*;
import java.util.*;

public class InputOutput {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        System.out.println(a);
        float b = sc.nextFloat();
        System.out.println(b);
        double c = sc.nextDouble();
        System.out.println(c);
    }
}
